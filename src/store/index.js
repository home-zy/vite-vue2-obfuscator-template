import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)

const store = new Vuex.Store({
  //提供唯一的公共数据源，所有共享数据都要统一放到State中进行存储,感觉类似于data
  state: {
    count: 0
  },
  //用于变更state中的数据，只能用它来操作state中的数据（里面不能使用一步操作）,感觉类似于methods
  mutations: {
    increment(state) {
      state.count++
    },
    decrement(state) {
      state.count--
    }
  },
  //和mutation功能大致相同，不同之处在于action提交的是mutation,而不是直接变更状态，可以包含异步操作
  actions: {
  },
  //从基本数据state派生的数据，相当于state的计算属性，具有返回值的方法，他会返回一个新数据，不会影响到state里面的老数据
  gettersL: {
  },
  //他把store拆分成了一个个小模块，是为了当store很大的时候方便管理，每个模块都有这state,mutations,actions,getters,4个属性
  modules: {
  }
})

export default store
