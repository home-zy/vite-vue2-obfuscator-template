
const path = require("path")
const JavaScriptObfuscator = require("javascript-obfuscator");

export default function vitePlugObfuscator(options) {
    function transform(code, id) {
        if (id.includes('vendor.')) {
            return;
        }
        if (id.includes("node_modules")) {
            return;
        }
        if (path.extname(id) == ".js") {
            let obfuscationResult = JavaScriptObfuscator.obfuscate(code, options);
            let result = { code: obfuscationResult.getObfuscatedCode() };

            if (options.sourceMap && options.sourceMapMode !== 'inline') {
                result.map = obfuscationResult.getSourceMap();
            }

            return result;
        }
    }
    return {
        name: "vite-plug-obfuscator",
        transform: transform
        // renderChunk(code, chunk, options) {
        //     console.log(chunk)
        //     const res = transform.call(this, code, chunk.fileName)
        //     if (res) {
        //         chunk.code=res.code;
        //         // Object.assign(chunk, res)
        //         return res.code
        //     }
        // }
    }
}