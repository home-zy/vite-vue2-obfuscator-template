const { createVuePlugin } = require('vite-plugin-vue2');
import vitePlugObfuscator from './plugs/vite-plug-obfuscator'

const plugins = [
  createVuePlugin(),
]
// console.log(process.env.NODE_ENV)
if (process.env.NODE_ENV == 'production' && true) {
  console.log("启用代码加密插件")
  plugins.push(vitePlugObfuscator({
    //指定的域名
    domainLock: ["localhost", "127.0.0.1"],
    //重定向域名
    domainLockRedirectUrl: "smbc.co.jp",
    compact: true,
    controlFlowFlattening: true,
    controlFlowFlatteningThreshold: 0.75,
    deadCodeInjection: true,
    deadCodeInjectionThreshold: 0.4,
    debugProtection: false,
    debugProtectionInterval: false,
    //禁止控制台输出
    disableConsoleOutput: false,
    identifierNamesGenerator: 'hexadecimal',
    log: false,
    numbersToExpressions: true,
    renameGlobals: false,
    selfDefending: true,
    simplify: true,
    //不能开启否则加载可能出错
    // splitStrings: true,
    // splitStringsChunkLength: 10,
    // stringArray: true,
    // stringArrayEncoding: ['base64'],
    stringArrayIndexShift: true,
    stringArrayRotate: true,
    stringArrayShuffle: true,
    stringArrayWrappersCount: 2,
    stringArrayWrappersChainedCalls: true,
    stringArrayWrappersParametersMaxCount: 4,
    stringArrayWrappersType: 'function',
    stringArrayThreshold: 0.75,
    transformObjectKeys: true,
    unicodeEscapeSequence: false,
    sourceMap: false
  }))
}

module.exports = {
  // base:"./",
  plugins: plugins,
};
